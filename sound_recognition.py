import numpy as np
import pyaudio
import time
import librosa
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import json
import math
import scipy as sc
#from sklearn.preprocessing import MinMaxScaler
#import cv2

class PrototypeLayer(tf.keras.layers.Layer):
  
    def __init__(self, prototypes=None, radiuses=None, **kwargs):
        super().__init__(**kwargs)
        self.centers     = tf.constant(prototypes, dtype=tf.float32)
        self.radiuses    = tf.constant(radiuses, dtype=tf.float32)
        self.dim         = tf.constant(prototypes.shape[1], dtype=tf.float32)
        self.num_classes = tf.constant(prototypes.shape[0], dtype=tf.float32)
        self.one         = tf.constant(1, dtype=tf.float32)

    def build(self, input_shape):
        super().build(input_shape)

    def call(self, embedding, mask=None):
        batch_size          = embedding.shape[0]
        if batch_size is None:
            batch_size = 1
        expanded_centers    = tf.expand_dims(self.centers, axis=0)   
        expanded_centers    = tf.repeat( expanded_centers, repeats=[batch_size], axis=0 )        
        expanded_embedding  = tf.expand_dims( embedding, axis=1 )
        expanded_embedding  = tf.repeat( expanded_embedding, repeats=[self.num_classes], axis=1 ) 
        distance_to_centers = tf.math.squared_difference( expanded_embedding, expanded_centers )
        distance_to_centers = tf.math.reduce_sum( distance_to_centers, axis=2 )
        
        self.u              = self.one-(distance_to_centers /self.radiuses)
        self.u              = tf.nn.relu(self.u)
        temperature         = 0.33
        self.u              = tf.nn.softmax(self.u/temperature)
        return self.u

    def compute_output_shape(self, input_shape):
        return tf.compat.v1.keras.backend.int_shape(self.u)
      
    def get_config(self):
        config = super().get_config().copy()
        config.update({
            'prototypes': self.centers.numpy(),
            'radiuses': self.radiuses.numpy(),
        })
        return config

def create_base_network(extractor, prototypes, radiuses):
    x = extractor.output
    out = PrototypeLayer(prototypes=prototypes, radiuses=radiuses)(x)
    return tf.keras.models.Model(inputs=extractor.input, outputs=out)

def ensure_sample_rate(original_sample_rate, waveform, desired_sample_rate=16000):
    """Resample waveform if required."""
    if original_sample_rate != desired_sample_rate:
        desired_length = int(round(float(len(waveform)) /
                               original_sample_rate * desired_sample_rate))
        waveform = sc.signal.resample(waveform, desired_length)
    return desired_sample_rate, waveform

#*************************************************************************************************************

path_to_primary_feature_extractor   = 'yamnet_1'
path_to_highlevel_feature_extractor = 'highlevel_feature_extractor.h5'
path_to_prototypes                  = 'prototypes.csv'
path_to_radiuses                    = 'radiuses.csv'
path_to_labels                      = 'labels.json'

primary_feature_extractor   = tf.saved_model.load(path_to_primary_feature_extractor)
highlevel_feature_extractor = tf.keras.models.load_model(path_to_highlevel_feature_extractor)
labels                      = {int(key) : value for key, value in json.loads(open(path_to_labels, "r").read()).items()}
radiuses                    = np.loadtxt(path_to_radiuses, delimiter=',')
prototypes                  = np.loadtxt(path_to_prototypes, delimiter=',')
classifier                  = create_base_network(highlevel_feature_extractor,
                                                  prototypes,
                                                  radiuses)
FRT       = pyaudio.paFloat32
CHAN      = 1 
RT        = 44100 
TARGET_RT = 16000
REC_SEC   = 1 

p = pyaudio.PyAudio()
stream = p.open(format=FRT,
                channels=CHAN,
                rate=RT,
                input=True,
                frames_per_buffer=int(RT*REC_SEC))

#scaler = MinMaxScaler()
while True:
    data = stream.read(int(RT*REC_SEC))
    signal = np.frombuffer(data, dtype=np.float32)
    signal = ensure_sample_rate(RT, signal, desired_sample_rate=TARGET_RT)[1]
    scores, embeddings, log_mel_spectrogram = primary_feature_extractor(signal)
    feature_vector = embeddings.numpy()
    feature_vector = feature_vector.reshape( feature_vector.shape[0]*feature_vector.shape[1])
    feature_vector = np.expand_dims(feature_vector, axis=0)
    pr = classifier.predict(feature_vector)[0]

    for o in range(len(labels)):
        if pr[o] > 0.8 :
            if labels[o] != "background":
                print(labels[o] + ", " + str(pr[o]*100) + "%" )
        
    print("---------------------------------------------")
    

    #heatmap = scaler.fit_transform(log_mel_spectrogram.numpy().T[:,0:100])
    #heatmap = heatmap*255
    #heatmap = heatmap.astype(np.uint8)
    #heatmap = cv2.applyColorMap(heatmap, cv2.COLORMAP_HOT) 
    #heatmap = cv2.resize(heatmap, (heatmap.shape[1]*5, heatmap.shape[0]*5), interpolation = cv2.INTER_AREA)
    #cv2.imshow('MEL Spectrogram', heatmap)
    #if cv2.waitKey(25) & 0xFF == ord('q'):
    #    break
    

stream.stop_stream() 
stream.close()
p.terminate()