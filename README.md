# DEMO : acoustic-smart-sensor 

## Getting started
1. Install Python
2. Install dependecies from requirements.txt.
3. Deploy and run sound_recognition.py.

## Authors and acknowledgment
Moskalenko Viacheslav from Molfar.Tech && Sumy State University.

## License
GPL v3 License.

## Project status
R&D continues for the purpose of acoustic sensor array fusion in order to determine the coordinates and class of a wide range of enemy objects
